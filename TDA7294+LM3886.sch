EESchema Schematic File Version 4
LIBS:TDA7294+LM3886-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Audio:TDA7294 U1
U 1 1 5B79CD49
P 3000 2850
F 0 "U1" H 3441 2896 50  0000 L CNN
F 1 "TDA7294" H 3250 2700 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 3000 2850 50  0001 C CIN
F 3 "http://www.st.com/resource/en/datasheet/tda7294.pdf" H 3000 2850 50  0001 C CNN
	1    3000 2850
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Audio:LM3886 U2
U 1 1 5B79CDD6
P 7900 2850
F 0 "U2" H 8241 2896 50  0000 L CNN
F 1 "LM3886" H 8050 2700 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-11_P3.4x5.08mm_StaggerOdd_Lead4.85mm_Vertical" H 7900 2850 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm3886.pdf" H 7900 2850 50  0001 C CNN
	1    7900 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5B7A4C03
P 3100 3300
F 0 "#PWR0101" H 3100 3050 50  0001 C CNN
F 1 "GND" H 3105 3127 50  0000 C CNN
F 2 "" H 3100 3300 50  0001 C CNN
F 3 "" H 3100 3300 50  0001 C CNN
	1    3100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3050 3100 3250
Wire Wire Line
	3000 3150 3000 3250
Wire Wire Line
	3000 3250 3100 3250
Connection ~ 3100 3250
Wire Wire Line
	3100 3250 3100 3300
$Comp
L power:VEE #PWR0102
U 1 1 5B7A4D98
P 2800 3400
F 0 "#PWR0102" H 2800 3250 50  0001 C CNN
F 1 "VEE" H 2818 3573 50  0000 C CNN
F 2 "" H 2800 3400 50  0001 C CNN
F 3 "" H 2800 3400 50  0001 C CNN
	1    2800 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	2800 3400 2800 3350
Wire Wire Line
	2900 3250 2900 3350
Wire Wire Line
	2900 3350 2800 3350
Connection ~ 2800 3350
Wire Wire Line
	2800 3350 2800 3250
$Comp
L power:VCC #PWR0103
U 1 1 5B7A4E3E
P 2800 1000
F 0 "#PWR0103" H 2800 850 50  0001 C CNN
F 1 "VCC" H 2817 1173 50  0000 C CNN
F 2 "" H 2800 1000 50  0001 C CNN
F 3 "" H 2800 1000 50  0001 C CNN
	1    2800 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2450 2900 2350
Wire Wire Line
	2900 2350 2800 2350
Connection ~ 2800 2350
Wire Wire Line
	2800 2350 2800 2450
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5B7A502E
P 2050 6600
F 0 "J2" H 2156 6878 50  0000 C CNN
F 1 "SUPPLY" H 2156 6787 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 2050 6600 50  0001 C CNN
F 3 "~" H 2050 6600 50  0001 C CNN
	1    2050 6600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 5B7A512E
P 2800 6350
F 0 "C4" H 2918 6396 50  0000 L CNN
F 1 "220u/50V" H 2918 6305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 2838 6200 50  0001 C CNN
F 3 "~" H 2800 6350 50  0001 C CNN
	1    2800 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C5
U 1 1 5B7A51B2
P 2800 6850
F 0 "C5" H 2918 6896 50  0000 L CNN
F 1 "220u/50V" H 2918 6805 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 2838 6700 50  0001 C CNN
F 3 "~" H 2800 6850 50  0001 C CNN
	1    2800 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6600 2800 6600
Wire Wire Line
	2800 6600 2800 6500
Wire Wire Line
	2800 6700 2800 6600
Connection ~ 2800 6600
Wire Wire Line
	2250 6500 2450 6500
Wire Wire Line
	2450 6500 2450 6150
Wire Wire Line
	2450 6150 2800 6150
Wire Wire Line
	2800 6150 2800 6200
Wire Wire Line
	2250 6700 2450 6700
Wire Wire Line
	2450 6700 2450 7050
Wire Wire Line
	2450 7050 2800 7050
Wire Wire Line
	2800 7050 2800 7000
$Comp
L Device:C C7
U 1 1 5B7A5C43
P 3450 6350
F 0 "C7" H 3565 6396 50  0000 L CNN
F 1 "100n" H 3565 6305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3488 6200 50  0001 C CNN
F 3 "~" H 3450 6350 50  0001 C CNN
	1    3450 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5B7A5CD1
P 3450 6850
F 0 "C8" H 3565 6896 50  0000 L CNN
F 1 "100n" H 3565 6805 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3488 6700 50  0001 C CNN
F 3 "~" H 3450 6850 50  0001 C CNN
	1    3450 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 6700 3450 6600
Wire Wire Line
	2800 6600 3450 6600
Connection ~ 3450 6600
Wire Wire Line
	3450 6600 3450 6500
Wire Wire Line
	2800 6150 3450 6150
Wire Wire Line
	3450 6150 3450 6200
Connection ~ 2800 6150
Wire Wire Line
	2800 7050 3450 7050
Wire Wire Line
	3450 7050 3450 7000
Connection ~ 2800 7050
$Comp
L Device:CP C11
U 1 1 5B7A6CDB
P 3950 6350
F 0 "C11" H 4068 6396 50  0000 L CNN
F 1 "220u/50V" H 4068 6305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 3988 6200 50  0001 C CNN
F 3 "~" H 3950 6350 50  0001 C CNN
	1    3950 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C12
U 1 1 5B7A6D11
P 3950 6850
F 0 "C12" H 4068 6896 50  0000 L CNN
F 1 "220u/50V" H 4068 6805 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 3988 6700 50  0001 C CNN
F 3 "~" H 3950 6850 50  0001 C CNN
	1    3950 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5B7A6D55
P 4600 6350
F 0 "C13" H 4715 6396 50  0000 L CNN
F 1 "100n" H 4715 6305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4638 6200 50  0001 C CNN
F 3 "~" H 4600 6350 50  0001 C CNN
	1    4600 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 5B7A6D95
P 4600 6850
F 0 "C14" H 4715 6896 50  0000 L CNN
F 1 "100n" H 4715 6805 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4638 6700 50  0001 C CNN
F 3 "~" H 4600 6850 50  0001 C CNN
	1    4600 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 6700 4600 6600
Wire Wire Line
	3950 6700 3950 6600
Wire Wire Line
	3450 6600 3950 6600
Connection ~ 3950 6600
Wire Wire Line
	3950 6600 3950 6500
Wire Wire Line
	3950 6600 4600 6600
Connection ~ 4600 6600
Wire Wire Line
	4600 6600 4600 6500
Wire Wire Line
	4600 6200 4600 6150
Wire Wire Line
	4600 6150 3950 6150
Connection ~ 3450 6150
Wire Wire Line
	3950 6200 3950 6150
Connection ~ 3950 6150
Wire Wire Line
	3950 6150 3450 6150
Wire Wire Line
	3450 7050 3950 7050
Wire Wire Line
	4600 7050 4600 7000
Connection ~ 3450 7050
Wire Wire Line
	3950 7000 3950 7050
Connection ~ 3950 7050
Wire Wire Line
	3950 7050 4600 7050
$Comp
L power:VCC #PWR0104
U 1 1 5B7AAC3F
P 4600 6100
F 0 "#PWR0104" H 4600 5950 50  0001 C CNN
F 1 "VCC" H 4617 6273 50  0000 C CNN
F 2 "" H 4600 6100 50  0001 C CNN
F 3 "" H 4600 6100 50  0001 C CNN
	1    4600 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 6100 4600 6150
Connection ~ 4600 6150
$Comp
L power:VEE #PWR0105
U 1 1 5B7AB58C
P 4600 7100
F 0 "#PWR0105" H 4600 6950 50  0001 C CNN
F 1 "VEE" H 4618 7273 50  0000 C CNN
F 2 "" H 4600 7100 50  0001 C CNN
F 3 "" H 4600 7100 50  0001 C CNN
	1    4600 7100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 7100 4600 7050
Connection ~ 4600 7050
$Comp
L Device:R R5
U 1 1 5B7AC2E9
P 3000 3950
F 0 "R5" V 2793 3950 50  0000 C CNN
F 1 "22k" V 2884 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2930 3950 50  0001 C CNN
F 3 "~" H 3000 3950 50  0001 C CNN
	1    3000 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 3950 3650 3950
Wire Wire Line
	3650 3950 3650 2850
Wire Wire Line
	3650 2850 3400 2850
Wire Wire Line
	2850 3950 2350 3950
Wire Wire Line
	2350 3950 2350 2950
Wire Wire Line
	2350 2950 2600 2950
$Comp
L Device:CP C9
U 1 1 5B7AE27A
P 3650 2600
F 0 "C9" H 3768 2646 50  0000 L CNN
F 1 "22u" H 3768 2555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 3688 2450 50  0001 C CNN
F 3 "~" H 3650 2600 50  0001 C CNN
	1    3650 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2650 3200 2350
Wire Wire Line
	3200 2350 3650 2350
Wire Wire Line
	3650 2350 3650 2450
Wire Wire Line
	3650 2750 3650 2850
Connection ~ 3650 2850
$Comp
L Device:C C1
U 1 1 5B7AFE72
P 1600 2750
F 0 "C1" V 1348 2750 50  0000 C CNN
F 1 "1u" V 1439 2750 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W3.5mm_P5.00mm" H 1638 2600 50  0001 C CNN
F 3 "~" H 1600 2750 50  0001 C CNN
	1    1600 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5B7AFF2A
P 1900 3050
F 0 "R1" H 1970 3096 50  0000 L CNN
F 1 "10k" H 1970 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1830 3050 50  0001 C CNN
F 3 "~" H 1900 3050 50  0001 C CNN
	1    1900 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5B7B1BEE
P 1900 3200
F 0 "#PWR0106" H 1900 2950 50  0001 C CNN
F 1 "GND" H 1905 3027 50  0000 C CNN
F 2 "" H 1900 3200 50  0001 C CNN
F 3 "" H 1900 3200 50  0001 C CNN
	1    1900 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5B7B1C4C
P 900 2750
F 0 "J1" H 1006 2928 50  0000 C CNN
F 1 "INPUT1" H 1006 2837 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 900 2750 50  0001 C CNN
F 3 "~" H 900 2750 50  0001 C CNN
	1    900  2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5B7B2B9C
P 1200 2950
F 0 "#PWR0107" H 1200 2700 50  0001 C CNN
F 1 "GND" H 1205 2777 50  0000 C CNN
F 2 "" H 1200 2950 50  0001 C CNN
F 3 "" H 1200 2950 50  0001 C CNN
	1    1200 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 2950 1200 2850
Wire Wire Line
	1200 2850 1100 2850
$Comp
L Device:CP C2
U 1 1 5B7B3EA7
P 2350 4600
F 0 "C2" V 2095 4600 50  0000 C CNN
F 1 "22u" V 2186 4600 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2388 4450 50  0001 C CNN
F 3 "~" H 2350 4600 50  0001 C CNN
	1    2350 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5B7B3F6B
P 2350 4200
F 0 "R3" V 2143 4200 50  0000 C CNN
F 1 "680" V 2234 4200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2280 4200 50  0001 C CNN
F 3 "~" H 2350 4200 50  0001 C CNN
	1    2350 4200
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 5B7BAEF7
P 6200 2750
F 0 "J4" H 6306 2928 50  0000 C CNN
F 1 "INPUT2" H 6306 2837 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6200 2750 50  0001 C CNN
F 3 "~" H 6200 2750 50  0001 C CNN
	1    6200 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C15
U 1 1 5B7BAF9F
P 6750 2750
F 0 "C15" V 6498 2750 50  0000 C CNN
F 1 "100n" V 6589 2750 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 6788 2600 50  0001 C CNN
F 3 "~" H 6750 2750 50  0001 C CNN
	1    6750 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5B7BB03D
P 7050 3050
F 0 "R8" H 7120 3096 50  0000 L CNN
F 1 "10k" H 7120 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6980 3050 50  0001 C CNN
F 3 "~" H 7050 3050 50  0001 C CNN
	1    7050 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5B7BB0C1
P 7050 3200
F 0 "#PWR0108" H 7050 2950 50  0001 C CNN
F 1 "GND" H 7055 3027 50  0000 C CNN
F 2 "" H 7050 3200 50  0001 C CNN
F 3 "" H 7050 3200 50  0001 C CNN
	1    7050 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 2900 7050 2750
Wire Wire Line
	7050 2750 6900 2750
Wire Wire Line
	6600 2750 6400 2750
$Comp
L Device:R R9
U 1 1 5B7A818D
P 7350 2750
F 0 "R9" H 7420 2796 50  0000 L CNN
F 1 "1k" H 7420 2705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7280 2750 50  0001 C CNN
F 3 "~" H 7350 2750 50  0001 C CNN
	1    7350 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	7500 2750 7600 2750
Wire Wire Line
	7200 2750 7050 2750
Connection ~ 7050 2750
$Comp
L Device:R R2
U 1 1 5B7AD2B6
P 2200 2750
F 0 "R2" H 2270 2796 50  0000 L CNN
F 1 "1k" H 2270 2705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2130 2750 50  0001 C CNN
F 3 "~" H 2200 2750 50  0001 C CNN
	1    2200 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 2750 2600 2750
Wire Wire Line
	2050 2750 1900 2750
Wire Wire Line
	1900 2900 1900 2750
Connection ~ 1900 2750
Wire Wire Line
	1900 2750 1750 2750
$Comp
L power:GND #PWR0109
U 1 1 5B7B10A0
P 6500 2900
F 0 "#PWR0109" H 6500 2650 50  0001 C CNN
F 1 "GND" H 6505 2727 50  0000 C CNN
F 2 "" H 6500 2900 50  0001 C CNN
F 3 "" H 6500 2900 50  0001 C CNN
	1    6500 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2900 6500 2850
Wire Wire Line
	6500 2850 6400 2850
$Comp
L Device:R R12
U 1 1 5B7B28E6
P 7900 3800
F 0 "R12" V 7693 3800 50  0000 C CNN
F 1 "22k" V 7784 3800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7830 3800 50  0001 C CNN
F 3 "~" H 7900 3800 50  0001 C CNN
	1    7900 3800
	0    1    1    0   
$EndComp
$Comp
L power:VEE #PWR0110
U 1 1 5B7B29EB
P 7800 3150
F 0 "#PWR0110" H 7800 3000 50  0001 C CNN
F 1 "VEE" H 7818 3323 50  0000 C CNN
F 2 "" H 7800 3150 50  0001 C CNN
F 3 "" H 7800 3150 50  0001 C CNN
	1    7800 3150
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR0111
U 1 1 5B7B2A26
P 7800 2550
F 0 "#PWR0111" H 7800 2400 50  0001 C CNN
F 1 "VCC" H 7817 2723 50  0000 C CNN
F 2 "" H 7800 2550 50  0001 C CNN
F 3 "" H 7800 2550 50  0001 C CNN
	1    7800 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5B7B4649
P 3000 1400
F 0 "R4" H 2850 1450 50  0000 L CNN
F 1 "10k" H 2800 1350 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2930 1400 50  0001 C CNN
F 3 "~" H 3000 1400 50  0001 C CNN
	1    3000 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1550 3000 1650
$Comp
L Device:R R6
U 1 1 5B7B765A
P 3100 1400
F 0 "R6" H 3000 1500 50  0000 C CNN
F 1 "22k" H 2984 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3030 1400 50  0001 C CNN
F 3 "~" H 3100 1400 50  0001 C CNN
	1    3100 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 2650 3100 1650
Wire Wire Line
	2800 1000 2800 1100
$Comp
L Device:CP_Small C3
U 1 1 5B7C2C90
P 2550 1850
F 0 "C3" H 2638 1896 50  0000 L CNN
F 1 "10u" H 2638 1805 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2550 1850 50  0001 C CNN
F 3 "~" H 2550 1850 50  0001 C CNN
	1    2550 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5B7C2F31
P 2550 1950
F 0 "#PWR0112" H 2550 1700 50  0001 C CNN
F 1 "GND" H 2555 1777 50  0000 C CNN
F 2 "" H 2550 1950 50  0001 C CNN
F 3 "" H 2550 1950 50  0001 C CNN
	1    2550 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 1750 2550 1650
Wire Wire Line
	2550 1650 3000 1650
Connection ~ 3000 1650
Wire Wire Line
	3000 1650 3000 2550
$Comp
L Device:CP_Small C6
U 1 1 5B7C47F6
P 3200 1850
F 0 "C6" H 3288 1896 50  0000 L CNN
F 1 "10u" H 3288 1805 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 3200 1850 50  0001 C CNN
F 3 "~" H 3200 1850 50  0001 C CNN
	1    3200 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5B7C4850
P 3200 1950
F 0 "#PWR0113" H 3200 1700 50  0001 C CNN
F 1 "GND" H 3205 1777 50  0000 C CNN
F 2 "" H 3200 1950 50  0001 C CNN
F 3 "" H 3200 1950 50  0001 C CNN
	1    3200 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1650 3200 1650
Wire Wire Line
	3200 1650 3200 1750
Connection ~ 3100 1650
Wire Wire Line
	3100 1650 3100 1550
Wire Wire Line
	3100 1250 3100 1100
Wire Wire Line
	3100 1100 3000 1100
Connection ~ 2800 1100
Wire Wire Line
	2800 1100 2800 2350
Wire Wire Line
	3000 1250 3000 1100
Connection ~ 3000 1100
Wire Wire Line
	3000 1100 2800 1100
Wire Wire Line
	7750 3800 7500 3800
Wire Wire Line
	7500 3800 7500 2950
Wire Wire Line
	7500 2950 7600 2950
Wire Wire Line
	8050 3800 8350 3800
Wire Wire Line
	8350 3800 8350 2850
Wire Wire Line
	8350 2850 8200 2850
$Comp
L Device:R R10
U 1 1 5B7D0A6D
P 7500 4050
F 0 "R10" H 7570 4096 50  0000 L CNN
F 1 "1k" H 7570 4005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7430 4050 50  0001 C CNN
F 3 "~" H 7500 4050 50  0001 C CNN
	1    7500 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 3900 7500 3800
Connection ~ 7500 3800
Wire Wire Line
	2350 4450 2350 4350
Wire Wire Line
	1100 2750 1450 2750
Wire Wire Line
	2350 4050 2350 3950
Connection ~ 2350 3950
$Comp
L power:GND #PWR0114
U 1 1 5B7DDEB1
P 2350 4750
F 0 "#PWR0114" H 2350 4500 50  0001 C CNN
F 1 "GND" H 2355 4577 50  0000 C CNN
F 2 "" H 2350 4750 50  0001 C CNN
F 3 "" H 2350 4750 50  0001 C CNN
	1    2350 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C16
U 1 1 5B7DE067
P 7500 4450
F 0 "C16" V 7245 4450 50  0000 C CNN
F 1 "22u" V 7336 4450 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 7538 4300 50  0001 C CNN
F 3 "~" H 7500 4450 50  0001 C CNN
	1    7500 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5B7DE0F3
P 7500 4600
F 0 "#PWR0115" H 7500 4350 50  0001 C CNN
F 1 "GND" H 7505 4427 50  0000 C CNN
F 2 "" H 7500 4600 50  0001 C CNN
F 3 "" H 7500 4600 50  0001 C CNN
	1    7500 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4300 7500 4200
$Comp
L Device:R R11
U 1 1 5B7E0E44
P 7900 1700
F 0 "R11" H 7800 1800 50  0000 C CNN
F 1 "22k" H 7784 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 7830 1700 50  0001 C CNN
F 3 "~" H 7900 1700 50  0001 C CNN
	1    7900 1700
	-1   0    0    1   
$EndComp
$Comp
L power:VEE #PWR0116
U 1 1 5B7E0EDE
P 7900 1450
F 0 "#PWR0116" H 7900 1300 50  0001 C CNN
F 1 "VEE" H 7918 1623 50  0000 C CNN
F 2 "" H 7900 1450 50  0001 C CNN
F 3 "" H 7900 1450 50  0001 C CNN
	1    7900 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 1450 7900 1550
Wire Wire Line
	7900 2550 7900 1950
$Comp
L Device:CP C17
U 1 1 5B7E7A78
P 8350 2200
F 0 "C17" H 8468 2246 50  0000 L CNN
F 1 "22u" H 8468 2155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 8388 2050 50  0001 C CNN
F 3 "~" H 8350 2200 50  0001 C CNN
	1    8350 2200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5B7EC255
P 8350 2350
F 0 "#PWR0117" H 8350 2100 50  0001 C CNN
F 1 "GND" H 8355 2177 50  0000 C CNN
F 2 "" H 8350 2350 50  0001 C CNN
F 3 "" H 8350 2350 50  0001 C CNN
	1    8350 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 2050 8350 1950
Wire Wire Line
	8350 1950 7900 1950
Connection ~ 7900 1950
Wire Wire Line
	7900 1950 7900 1850
$Comp
L power:GND #PWR0118
U 1 1 5B7EEAF6
P 7900 3150
F 0 "#PWR0118" H 7900 2900 50  0001 C CNN
F 1 "GND" H 8050 3100 50  0000 C CNN
F 2 "" H 7900 3150 50  0001 C CNN
F 3 "" H 7900 3150 50  0001 C CNN
	1    7900 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5B7EED42
P 8600 3050
F 0 "R13" H 8670 3096 50  0000 L CNN
F 1 "2.7R/1W" H 8670 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8530 3050 50  0001 C CNN
F 3 "~" H 8600 3050 50  0001 C CNN
	1    8600 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C18
U 1 1 5B7EEEAD
P 8600 3450
F 0 "C18" V 8348 3450 50  0000 C CNN
F 1 "100n" V 8439 3450 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 8638 3300 50  0001 C CNN
F 3 "~" H 8600 3450 50  0001 C CNN
	1    8600 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	8600 3300 8600 3200
Wire Wire Line
	8600 2900 8600 2850
Wire Wire Line
	8600 2850 8350 2850
Connection ~ 8350 2850
$Comp
L power:GND #PWR0119
U 1 1 5B7F3C2C
P 8600 3600
F 0 "#PWR0119" H 8600 3350 50  0001 C CNN
F 1 "GND" H 8750 3550 50  0000 C CNN
F 2 "" H 8600 3600 50  0001 C CNN
F 3 "" H 8600 3600 50  0001 C CNN
	1    8600 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5B7F3E27
P 9200 2850
F 0 "R14" V 8993 2850 50  0000 C CNN
F 1 "10R/1W" V 9084 2850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9130 2850 50  0001 C CNN
F 3 "~" H 9200 2850 50  0001 C CNN
	1    9200 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	9050 2850 8600 2850
Connection ~ 8600 2850
$Comp
L Device:L L1
U 1 1 5B7F6890
P 9200 2500
F 0 "L1" V 9390 2500 50  0000 C CNN
F 1 "0.7uH" V 9299 2500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9200 2500 50  0001 C CNN
F 3 "~" H 9200 2500 50  0001 C CNN
	1    9200 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9050 2500 8600 2500
Wire Wire Line
	8600 2500 8600 2850
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 5B7F93E4
P 9850 2950
F 0 "J5" H 9823 2830 50  0000 R CNN
F 1 "OUT2" H 9823 2921 50  0000 R CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 9850 2950 50  0001 C CNN
F 3 "~" H 9850 2950 50  0001 C CNN
	1    9850 2950
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5B7FC1AF
P 4800 2950
F 0 "J3" H 4773 2830 50  0000 R CNN
F 1 "OUT1" H 4773 2921 50  0000 R CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4800 2950 50  0001 C CNN
F 3 "~" H 4800 2950 50  0001 C CNN
	1    4800 2950
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5B7FEC67
P 4450 3050
F 0 "#PWR0120" H 4450 2800 50  0001 C CNN
F 1 "GND" H 4455 2877 50  0000 C CNN
F 2 "" H 4450 3050 50  0001 C CNN
F 3 "" H 4450 3050 50  0001 C CNN
	1    4450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3050 4450 2950
Wire Wire Line
	4450 2950 4600 2950
$Comp
L Device:R R7
U 1 1 5B8042E8
P 3900 3150
F 0 "R7" H 3970 3196 50  0000 L CNN
F 1 "2.7R/1W" H 3970 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3830 3150 50  0001 C CNN
F 3 "~" H 3900 3150 50  0001 C CNN
	1    3900 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5B8043B2
P 3900 3550
F 0 "C10" V 3648 3550 50  0000 C CNN
F 1 "100n" V 3739 3550 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 3938 3400 50  0001 C CNN
F 3 "~" H 3900 3550 50  0001 C CNN
	1    3900 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 3400 3900 3300
$Comp
L power:GND #PWR0121
U 1 1 5B80FD3E
P 3900 3700
F 0 "#PWR0121" H 3900 3450 50  0001 C CNN
F 1 "GND" H 3905 3527 50  0000 C CNN
F 2 "" H 3900 3700 50  0001 C CNN
F 3 "" H 3900 3700 50  0001 C CNN
	1    3900 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3000 3900 2850
Wire Wire Line
	3900 2850 3650 2850
Wire Wire Line
	3900 2850 4600 2850
Connection ~ 3900 2850
Wire Wire Line
	9650 2850 9500 2850
Wire Wire Line
	9350 2500 9500 2500
Wire Wire Line
	9500 2500 9500 2850
Connection ~ 9500 2850
Wire Wire Line
	9500 2850 9350 2850
$Comp
L power:GND #PWR0122
U 1 1 5B81C67B
P 9500 3100
F 0 "#PWR0122" H 9500 2850 50  0001 C CNN
F 1 "GND" H 9650 3050 50  0000 C CNN
F 2 "" H 9500 3100 50  0001 C CNN
F 3 "" H 9500 3100 50  0001 C CNN
	1    9500 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 3100 9500 2950
Wire Wire Line
	9500 2950 9650 2950
$Comp
L power:GND #PWR0123
U 1 1 5B82539A
P 5100 6750
F 0 "#PWR0123" H 5100 6500 50  0001 C CNN
F 1 "GND" H 5105 6577 50  0000 C CNN
F 2 "" H 5100 6750 50  0001 C CNN
F 3 "" H 5100 6750 50  0001 C CNN
	1    5100 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 6750 5100 6600
Wire Wire Line
	5100 6600 4600 6600
$EndSCHEMATC
